<?php

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Schema;
use Hypersoft\DBAL\Firebird\FirebirdDriver;
use PHPUnit\Framework\TestCase;

class DbalFirebirdTest extends TestCase
{
    /**
     * @var Doctrine\DBAL\Connection
     */
    private $connection = null;

    /**
     * @var boolean
     */
    private static $databaseFresh = false;

    protected function setUp(): void
    {
        if (!self::$databaseFresh) {
            copy(__DIR__ . '/EMPTY.FDB', __DIR__ . '/DATA.FDB');
            self::$databaseFresh = true;
        }
        $this->connection = DriverManager::getConnection([
            'driverClass' => FirebirdDriver::class,
            'dbname' => realpath(__DIR__ . '/DATA.FDB'),
            'user' => 'SYSDBA',
            'password' => 'masterkey'
        ]);
    }

    public function testCreateSchema(): Schema
    {
        $schema = $this->connection->createSchemaManager()->createSchema();
        $schema->createSequence('gen_test_table_id');
        $testTable = $schema->createTable('test_table');
        $testTable->addColumn('id', 'integer');
        $testTable->addColumn('text_field', 'string');
        $testTable->addColumn('float_field', 'float');
        $testTable->setPrimaryKey(['id']);

        foreach ($schema->toSql($this->connection->getDatabasePlatform()) as $sql) {
            $this->assertEquals(0, $this->connection->executeStatement($sql));
        }

        return $schema;
    }

    /**
     * @depends testCreateSchema
     */
    public function testAlterSchema(Schema $schema)
    {
        // $schema = $this->connection->getSchemaManager()->createSchema();
        $newSchema = clone $schema;
        $testTable = $newSchema->getTable('test_table');
        $testTable->addColumn('date_field', 'datetime');

        $newSchema->createSequence('gen_joined_table_id');
        $joinedTable = $newSchema->createTable('joined_table');
        $joinedTable->addColumn('id', 'integer');
        $joinedTable->addColumn('test_table_id', 'integer');
        $joinedTable->addColumn('useless_field', 'string', ['length' => 10]);

        foreach ($newSchema->getMigrateFromSql($schema, $this->connection->getDatabasePlatform()) as $sql) {
            $this->assertEquals(0, $this->connection->executeStatement($sql));
        }

        return $newSchema;
    }

    /**
     * @depends testAlterSchema
     */
    public function testGenIdAndInsertRecords(Schema $_): array
    {
        $testTableIdSql = $this->connection->getDatabasePlatform()->getSequenceNextValSQL('gen_test_table_id');
        $testTableId = $this->connection->fetchOne($testTableIdSql);
        $this->assertEquals(
            1,
            $this->connection->insert('test_table', ['id' => $testTableId, 'text_field' => 'charset áéíóú', 'float_field' => 3.14, 'date_field' => date('Y-m-d H:i:s')])
        );

        $joinedTableIdSql = $this->connection->getDatabasePlatform()->getSequenceNextValSQL('gen_joined_table_id');
        $joinedTableId = $this->connection->fetchOne($joinedTableIdSql);
        $this->assertEquals(
            1,
            $this->connection->insert('joined_table', ['id' => $joinedTableId, 'test_table_id' => $testTableId, 'useless_field' => 'judity'])
        );

        return [
            'testTableId' => $testTableId,
            'joinedTableId' => $joinedTableId,
        ];
    }

    /**
     * @depends testGenIdAndInsertRecords
     */
    public function testQueryBuilder(array $recordsIds)
    {
        $stm = $this->connection->createQueryBuilder()
            ->select('j.id, j.useless_field, t.text_field')
            ->from('test_table', 't')
            ->innerJoin('t', 'joined_table', 'j', 'j.test_table_id = t.id')
            ->where('t.id = :id')
            ->setParameter('id', $recordsIds['testTableId'])
            ->executeQuery();

        $record = $stm->fetchAssociative();
        $this->assertIsArray($record);
        $this->assertEquals($recordsIds['joinedTableId'], $record['id']);
        $this->assertEquals('charset áéíóú', $record['text_field']);
        $this->assertEquals('judity', $record['useless_field']);
    }

    /**
     * @depends testQueryBuilder
     */
    public function testRollBackTransaction()
    {
        $this->connection->beginTransaction();
        $this->connection->executeQuery('DELETE FROM joined_table WHERE id = 1');
        $this->connection->rollBack();

        $this->assertEquals(1, $this->connection->fetchOne('SELECT count(*) FROM joined_table WHERE id = 1'));
    }
}
