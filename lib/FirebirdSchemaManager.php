<?php

namespace Hypersoft\DBAL\Firebird;

use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Doctrine\DBAL\Schema\Sequence;
use Doctrine\DBAL\Types\Type;

class FirebirdSchemaManager extends AbstractSchemaManager
{
    /**
     * {@inheritDoc}
     */
    protected function _getPortableSequenceDefinition($sequence)
    {
        return new Sequence(trim($sequence['rdb$generator_name']), 1, 0);
    }

    /**
     * {@inheritDoc}
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {
        $dbtype = strtolower(trim($tableColumn['type']));

        $name = strtolower(trim($tableColumn['name']));
        $type = Type::getType(
            $this->_platform->getDoctrineTypeMapping($dbtype)
        );
        $options = [
            'default'        => $tableColumn['default_content'],
            'notnull'        => (bool) $tableColumn['null_flag'],
            'length'         => ($dbtype == 'varying' || $dbtype == 'text' ? $tableColumn['str_len'] : null),
            'precision'      => ($dbtype == 'numeric' || $dbtype == 'decimal' ? $tableColumn['prec'] : null),
            'scale'          => ($dbtype == 'numeric' || $dbtype == 'decimal' ? $tableColumn['scale'] : null),
            'fixed'          => ($dbtype == 'text'),
            'unsigned'       => false,
            'autoincrement'  => false,
            'comment'        => $tableColumn['comments'],
            'platformOptions' => ( $dbtype == 'varying' || $dbtype == 'text' 
                                 ? ['collation' => $tableColumn['charset']]
                                 : []),
        ];

        return new Column($name, $type, $options);
    }

    /**
     * {@inheritDoc}
     */
    protected function _getPortableTableDefinition($table)
    {
        return strtolower($table['table_name']);
    }

    /**
     * {@inheritDoc}
     */
    protected function _getPortableTableForeignKeyDefinition($tableForeignKey)
    {
        return new ForeignKeyConstraint(
            [$tableForeignKey['local_column']],
            $tableForeignKey['foreign_table'],
            [$tableForeignKey['foreign_column']]
        );
    }
}
