<?php

namespace Hypersoft\DBAL\Firebird;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Driver\API\ExceptionConverter;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class FirebirdDriver implements Driver
{
    /**
     * {@inheritDoc}
     */
    public function connect(array $params)
    {
        $params = array_merge(
            [
                'host' => 'localhost',
                'charset' => 'utf8',
                'port' => 3050,
            ],
            $params
        );
        $dsn = sprintf(
            "firebird:dbname=%s/%d:%s;charset=%s",
            $params['host'],
            $params['port'],
            $params['dbname'],
            $params['charset']
        );

        return new FirebirdPdoConnection(
            $dsn,
            $params['user'],
            $params['password'],
            [\PDO::ATTR_CASE => \PDO::CASE_LOWER]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getDatabasePlatform()
    {
        return new FirebirdPlatform();
    }

    /**
     * {@inheritDoc}
     */
    public function getSchemaManager(Connection $conn, AbstractPlatform $platform)
    {
        return new FirebirdSchemaManager($conn, $platform);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'pdo_firebird';
    }

    /**
     * {@inheritDoc}
     */
    public function getDatabase(Connection $conn)
    {
        return $conn->getParams()['dbname'];
    }

    public function getExceptionConverter(): ExceptionConverter
    {
        return new FirebirdExceptionConverter();
    }
}
