<?php

namespace Hypersoft\DBAL\Firebird;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\Sequence;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\TableDiff;

class FirebirdPlatform extends AbstractPlatform
{
    /**
     * @var array|null
     */
    protected $doctrineTypeMapping = null;

    /**
     * {@inheritDoc}
     */
    public function getBooleanTypeDeclarationSQL(array $columnDef)
    {
        return 'SMALLINT';
    }

    /**
     * {@inheritDoc}
     */
    public function getDateTypeDeclarationSQL(array $fieldDeclaration)
    {
        return 'DATE';
    }

    /**
     * {@inheritDoc}
     */
    public function getTimeTypeDeclarationSQL(array $fieldDeclaration)
    {
        return 'TIME';
    }

    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $fieldDeclaration)
    {
        return 'TIMESTAMP';
    }

    /**
     * {@inheritDoc}
     */
    public function supportsSequences()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getIntegerTypeDeclarationSQL(array $columnDef)
    {
        return 'INTEGER' . $this->_getCommonIntegerTypeDeclarationSQL($columnDef);
    }

    /**
     * {@inheritDoc}
     */
    public function getBigIntTypeDeclarationSQL(array $columnDef)
    {
        return 'BIGINT';
    }

    /**
     * {@inheritDoc}
     */
    public function getSmallIntTypeDeclarationSQL(array $columnDef)
    {
        return 'SMALLINT';
    }

    /**
     * {@inheritDoc}
     */
    public function getClobTypeDeclarationSQL(array $field)
    {
        return 'BLOB SUB_TYPE TEXT SEGMENT SIZE 80';
    }

    /**
     * {@inheritDoc}
     */
    public function getBlobTypeDeclarationSQL(array $field)
    {
        return 'BLOB SUB_TYPE BINARY SEGMENT SIZE 80';
    }

    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed)
    {
        if ($length > 4096) {
            return $this->getClobTypeDeclarationSQL([]);
        }

        if ($fixed) {
            return 'CHAR' . ($length ? '(' . $length . ')' : '');
        }

        return 'VARCHAR' . ($length ? '(' . $length . ')' : '');
    }

    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $columnDef)
    {
        return '';
    }

    /**
     * {@inheritDoc}
     */
    protected function _getCreateTableSQL($tableName, array $columns, array $options = array())
    {
        $columnListSql = $this->getColumnDeclarationListSQL($columns);

        if (isset($options['uniqueConstraints']) && !empty($options['uniqueConstraints'])) {
            foreach ($options['uniqueConstraints'] as $name => $definition) {
                $columnListSql .= ', ' . $this->getUniqueConstraintDeclarationSQL($name, $definition);
            }
        }

        if (isset($options['primary']) && !empty($options['primary'])) {
            // ++
            foreach (array_unique(array_values($options['primary'])) as $pk_field) {
                $columnListSql .= ', CONSTRAINT PK_' . strtoupper($tableName) . '_' . strtoupper($pk_field) . ' PRIMARY KEY(' . $pk_field . ')';
            }
        }

        if (isset($options['indexes']) && !empty($options['indexes'])) {
            foreach ($options['indexes'] as $index => $definition) {
                $columnListSql .= ', ' . $this->getIndexDeclarationSQL($index, $definition);
            }
        }

        $query = 'CREATE TABLE ' . $tableName . ' (' . $columnListSql . ')';

        $sql[] = $query;

        if (isset($options['foreignKeys'])) {
            foreach ((array) $options['foreignKeys'] as $definition) {
                $sql[] = $this->getCreateForeignKeySQL($definition, $tableName);
            }
        }

        return $sql;
    }

    public function getAlterTableSQL(TableDiff $diff)
    {
        $tableName = $diff->name;
        $columnSql = array();
        $queryParts = array();
        if ($diff->newName !== false) {
            throw new DBALException('Plataforma não permite alterar nome de tabela.');
        }

        if (!empty($diff->changedColumns)) {
            throw new DBALException('Plataforma não permite alterar tipos de colunas.');
        }

        if (!empty($diff->renamedColumns)) {
            throw new DBALException('Plataforma não permite alterar nomes de colunas.');
        }

        foreach ($diff->addedColumns as $column) {
            $columnArray = $column->toArray();
            $columnArray['comment'] = $this->getColumnComment($column);
            $queryParts[] = 'ALTER TABLE ' . $tableName . ' ADD ' . $this->getColumnDeclarationSQL($column->getQuotedName($this), $columnArray);
        }

        foreach ($diff->removedColumns as $column) {
            $queryParts[] = 'ALTER TABLE ' . $tableName . ' DROP ' . $column->getName();
        }

        // foreach ($diff->addedIndexes as $index) {
        //     $queryParts[] = 'ALTER TABLE '
        // }

        return $queryParts;
    }

    /**
     * {@inheritDoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
        $this->doctrineTypeMapping = array(
            'bigint' => 'bigint',
            'blob' => 'blob',
            'clob' => 'text',
            'date' => 'date',
            'decimal' => 'float',
            'double' => 'float',
            'float' => 'float',
            'long' => 'integer',
            'numeric' => 'float',
            'short' => 'smallint',
            'text' => 'string',
            'time' => 'time',
            'timestamp' => 'datetime',
            'varying' => 'string'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'pdo_firebird';
    }

    /**
     * {@inheritDoc}
     */
    public function prefersSequences()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getListSequencesSQL($database)
    {
        return 'SELECT RDB$GENERATOR_NAME FROM RDB$GENERATORS WHERE RDB$SYSTEM_FLAG = 0';
    }

    /**
     * {@inheritDoc}
     */
    public function getCreateSequenceSQL(Sequence $sequence)
    {
        return 'CREATE GENERATOR ' . $sequence->getName();
    }

    /**
     * {@inheritDoc}
     */
    public function getSequenceNextValSQL($sequenceName)
    {
        return 'SELECT NEXT VALUE FOR ' . $sequenceName . ' FROM RDB$DATABASE';
    }

    /**
     * {@inheritDoc}
     */
    public function getListTableColumnsSQL($table, $database = null)
    {
        return 'select
            rf.rdb$field_name as "name",
            case t.rdb$type_name
                when \'BLOB\' then
                    case f.rdb$field_sub_type
                        when 0 then \'BLOB\'
                        when 1 then \'CLOB\'
                    end
                when \'INT64\' then
                    case f.rdb$field_sub_type
                        when 0 then \'BIGINT\'
                        when 1 then \'NUMERIC\'
                        when 2 then \'DECIMAL\'
                    end
                else t.rdb$type_name
            end as "type",
            f.rdb$field_precision as "prec",
            f.rdb$field_scale * -1 as "scale",
            f.rdb$character_length as "str_len",
            rf.rdb$null_flag as "null_flag",
            rf.rdb$default_value as "default_content",
            rf.rdb$description as "comments",
            cs.rdb$character_set_name as "charset"
        from rdb$relation_fields as rf
        inner join rdb$fields as f on f.rdb$field_name = rf.rdb$field_source
        inner join rdb$types as t on t.rdb$type = f.rdb$field_type and t.rdb$field_name = \'RDB$FIELD_TYPE\'
        left join rdb$character_sets cs on cs.rdb$character_set_id = f.rdb$character_set_id
        where rf.rdb$relation_name = \'' . strtoupper($table) . '\'
        order by rdb$field_position';
    }

    /**
     * {@inheritDoc}
     */
    public function getListTableIndexesSQL($table, $currentDatabase = null)
    {
        return 'select
            trim(rc.rdb$index_name) as "key_name",
            trim(seg.rdb$field_name) as "column_name",
            1 as "primary",
            case
                when i.rdb$unique_flag is null then 0
                else i.rdb$unique_flag
            end as "non_unique"
        from rdb$relation_constraints as rc
        inner join rdb$index_segments as seg on seg.rdb$index_name = rc.rdb$index_name
        inner join rdb$indices as i on i.rdb$index_name = rc.rdb$index_name and i.rdb$relation_name = rc.rdb$relation_name
        where
            rc.rdb$constraint_type = \'PRIMARY KEY\' and upper(rc.rdb$relation_name) = \'' . strtoupper($table) . '\'';
    }

    /**
     * {@inheritDoc}
     */
    public function getListTableForeignKeysSQL($table)
    {
        return 'select
            (select trim(rdb$field_name) from rdb$index_segments where rdb$index_name = rlc.rdb$constraint_name) as "local_column",
            (select trim(rdb$relation_name) from rdb$relation_constraints where rdb$constraint_name = rfc.rdb$const_name_uq) as "foreign_table",
            (select trim(rdb$field_name) from rdb$index_segments where rdb$index_name = rfc.rdb$const_name_uq) as "foreign_column",
            trim(rlc.rdb$constraint_name) as "index_name",
            trim(rfc.rdb$update_rule) as "on_update",
            trim(rfc.rdb$delete_rule) as "on_delete"
        from rdb$relation_constraints as rlc
        inner join rdb$ref_constraints as rfc on rfc.rdb$constraint_name = rlc.rdb$constraint_name
        where rlc.rdb$constraint_type = \'FOREIGN KEY\' and upper(rlc.rdb$relation_name) = \'' . strtoupper($table) . '\'';
    }

    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
        return 'select trim(rdb$relation_name) as "table_name" from rdb$relations where rdb$system_flag = 0';
    }

    /**
     * {@inheritDoc}
     */
    protected function doModifyLimitQuery($query, $limit, $offset)
    {
        // sqlite:   SELECT * FROM table LIMIT 10 OFFSET 10
        // firebird: SELECT * FROM table ROWS 11 TO 20
        $limit = (int) $limit;
        $offset = (int) $offset;
        if ($offset && $limit) {
            $query .= ' ROWS ' . ($offset + 1) . ' TO ' . ($offset + $limit);
        } elseif ($limit) {
            $query .= ' ROWS ' . $limit;
        }

        return $query;
    }

    /**
     * {@inheritDoc}
     */
    protected function getReservedKeywordsClass()
    {
        return FirebirdKeywords::class;
    }

    public function getCurrentDatabaseExpression(): string
    {
        return "''";
    }

    public function getDummySelectSQL()
    {
        $expression = func_num_args() > 0 ? func_get_arg(0) : '1';

        return sprintf('SELECT %s FROM rdb$database', $expression);
    }
}
